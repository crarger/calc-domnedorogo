$(document).ready(()=>{
    $('span.money').text(splitStr(calc()));

    $('div.calculator').click((event)=>{
         $('span.money').text(splitStr(calc()));
    });

});


/**
 * считаем периметр
 */
function perimeter(lenght, width) {
    return 2 * (lenght + width);
}

/**
 * считаем площпдь
 */
function area(lenght, width) {
    return lenght * width;
}

/**
 * считаем ленточный фундамент
 */
function lentFoundation(lenght, width) {
    return (((2 * lenght + 3 * width) * 0.24) + 2) * 11;
}

/**
 * считаем фундамент монолитная плита
 */
function monolithFoundation(lenght, width) {
    return (lenght * width) * 0.3 * 11 + 100000;
}

/**
 * считаем свайно-забивной фундамент
 */
function szFoundation(lenght, width) {
    return ((lenght / 3) + 1) + ((width / 3) + 1) * 5500;
}

/**
 * считаем свайно-винтовой фундамент
 */
function svFoundation(lenght, width) {
    return ((lenght / 3) + 1) + ((width / 3) + 1) * 3500;
}

/**
 * проверяем выбранный тип фундамента и возвращаем стоимость
 */
function foundat(foundation, sizeLenght, sizeWidth) {
    switch(foundation) {
        case "Pile-screw":
            return svFoundation(sizeLenght, sizeWidth);
            break;
        case "monolith":
            return monolithFoundation(sizeLenght, sizeWidth);
            break;
        case "Pile-driving":
            return szFoundation(sizeLenght, sizeWidth);
            break;
        default:
            return lentFoundation(sizeLenght, sizeWidth);
    }
}

/**
 * считаем материал стен кирпич
 */
function wallsBrick(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 9000) +
        (perimeter(lenght, width) * (2.5 + height) * 5000) + (area(lenght, width) * 1.7 * 1600) +
        (area(lenght, width) * countFloor * 1400);
}

/**
 * считаем материал стен клееный брус
 */
function wallsGluedBeam(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 5150) +
        (perimeter(lenght, width) * (2.5 + height) * 5150) + (area(lenght, width) * 1.7 * 1600) +
        (area(lenght, width) * countFloor * 1400);
}

/**
 * считаем материал стен профилированный брус
 */
function wallsProfiledBeam(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 2150) +
        (perimeter(lenght, width) * (2.5 + height) * 2150) + (area(lenght, width) * 1.7 * 1600) +
        (area(lenght, width) * countFloor * 1400);
}

/**
 * считаем материал стен каркас
 */
function wallsKarcass(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * ( countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 950) +
        (perimeter(lenght, width) * (2.5 + height) * 730) + (area(lenght, width) * 1.7 * 2100) +
        (area(lenght, width) * (countFloor + 1) * 1355);
}

/**
 * считаем материал стен блоки
 */
function wallsBlocks(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 2120) +
        (perimeter(lenght, width) * (2.5 + height) * 1040) + (area(lenght, width) * 1.7 * 2500) +
        (area(lenght, width) * (countFloor + 1) * 1555);
}


/**
 * проверяем выбранный материал стен и возвращаем стоимость
 */
function wallsMaterial(material, lenght, width, height, countFloor) {
    switch(material) {
        case "Glued-beams":
            return wallsGluedBeam(lenght, width, height, countFloor);
            break;
        case "Blocks":
            return wallsBlocks(lenght, width, height, countFloor);
            break;
        case "Brick":
            return wallsBrick(lenght, width, height, countFloor);
            break;
        case "Profiled-beam":
            return wallsProfiledBeam(lenght, width, height, countFloor);
            break;
        default:
            return wallsKarcass(lenght, width, height, countFloor);
    }
}


/**
 * считаем внешнюю отделку Штукатурка
 */
function putty(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 920);
}

/**
 * считаем внешнюю отделку Штукатурка
 */
function osb(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 430);
}

/**
 * считаем внешнюю отделку Имитация бруса
 */
function imitationBar(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 1010);
}

/**
 * считаем внешнюю отделку Японские панели
 */
function japanPanele(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 1700);
}

/**
 * считаем внешнюю отделку Сайдинг
 */
function siding(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 900);
}

/**
 * считаем внешнюю отделку Хауберг
 */
function hauberg(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 1250);
}

/**
 * считаем внешнюю отделку Облицовка кирпичом
 */
function facingBrick(lenght, width, height, countFloor) {
    return ((((perimeter(lenght, width) * (countFloor * height + 0.6))) + ((Math.pow(width,2)) * 0.5)) * 2200);
}


/**
 * проверяем выбранный тип отделки и возвращаем стоимость
 */
function decoration(exterierDecoration, lenght, width, height, countFloor) {
    switch(exterierDecoration) {
        case "Simulation-bar":
            return imitationBar(lenght, width, height, countFloor);
            break;
        case "Japan-panele":
            return japanPanele(lenght, width, height, countFloor);
            break;
        case "Siding":
            return siding(lenght, width, height, countFloor);
            break;
        case "Hauberg":
            return hauberg(lenght, width, height, countFloor);
            break;
        case "Brick":
            return facingBrick(lenght, width, height, countFloor);
            break;
        case "osb":
            return facingBrick(lenght, width, height, countFloor);
            break;
        default:
            return putty(lenght, width, height, countFloor);
    }
}

/**
 * считаем стоимость окон
 */
function windowsPrice(lenght, width, height, countFloor) {
    return ((perimeter(lenght, width) * (countFloor * height + 0.6)) * 800);
}

/**
 * считаем чекбоксы
 */
function checks(stairs, ironDoor, windows, communications, lenght, width, height, countFloor) {
    let sum = 0;

    if(stairs) {
        sum = sum + 70000;
    }
    if(ironDoor){
        sum = sum + 40000;
    }

    if(windows){
        sum = sum + windowsPrice(lenght, width, height, countFloor);
    }

    if(communications){
        sum = sum + 400000;
    }

    return sum;
}


/**
 * проверяем выбранный материал стен и решаем брать ли в рассчет отделку
 */
function selectMaterial( material, exterierDecoration, lenght, width, height, countFloor) {
    switch (material) {
        case "Blocks":
        case "Frame":
            $('.row-decoration').show();
            return wallsMaterial(material, lenght, width, height, countFloor) +
                decoration(exterierDecoration, lenght, width, height, countFloor);
            break;
        default:
            $('.row-decoration').hide();
            return wallsMaterial(material, lenght, width, height, countFloor);
            break;
    }
}

function splitStr(value) {
    return value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, `$1 `);
}

/**
 * суммируем все полученные данные и округляем их до целочисленных
 */
function calc() {
    let foundation = $('select[name=foundation] option:selected').val();
    let exterierDecoration = $('select[name=exterierDecoration]').val();
    let wMaterial = $('select[name=wallsMaterial]').val();
    let sizeLenght = parseInt($('input[name=sizeLenght]').val());
    let sizeWidth = parseInt($('input[name=sizeWidth]').val());
    let floorHeight = parseInt($('select[name=heightFloor]').val());
    let countFloor = parseInt($('select[name=floor] option:selected').val(), 10);
    let stairs = $('input[name=stairs]').is(':checked');
    let windows = $('input[name=windows]').is(':checked');
    let ironDoor = $('input[name=ironDoor]').is(':checked');
    let communications = $('input[name=communications]').is(':checked');

    return (foundat(foundation, sizeLenght, sizeWidth) +
        selectMaterial(wMaterial, exterierDecoration, sizeLenght, sizeWidth, floorHeight, countFloor) +
        checks(stairs, ironDoor, windows, communications, sizeLenght, sizeWidth, floorHeight, countFloor) +
        400000).toFixed(0);
}